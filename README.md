# librfm69hw

A (hopefully) microcontroller agnostic driver for the RFM69HW transceiver module implemented in plain C.

Files that you should be aware off:

  * ./src --> source code.
  * ./doc --> doxyfile and generated documentation ( .gitignored ).
  * ./doc/librfm69hw.Doxyfile --> doxygen configuration file.
  * ./README.md --> this file.

Licensed under the [GPLv3](LICENSE).