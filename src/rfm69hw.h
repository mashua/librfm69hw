#ifndef RFM69HW_H
#define RFM69HW_H

typedef enum {
    FixedLen = 0,
    VariableLen =1,
    LastLen
} PacketLength;

typedef enum {
    /*multiples of 1.2 kbps*/
    kbps1_2   = 0,
    kbps2_4   = 1,
    kbps4_8   = 2,
    kbps9_6   = 3,
    kbps19_2  = 4,
    kbps38_4  = 5,
    kbps76_8  = 6,
    kbps153_6 = 7,
    /*multiples of 0.9 kbps*/
    kbps57_6  = 8,
    kbps115_2 = 9,
    /*multiples of 12.5, 25 and 50 kbps*/
    kbps12_5  = 10,
    kbps25    = 11,
    kbps50    = 12,
    kbps100   = 13,
    kbps150   = 14,
    kbps200   = 15,
    kbps250   = 16,
    kbps300   = 17,
    /*watch Xtal frequency*/
    kbps32_768= 18
} BitRate;


#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_spi.h"

#include "rfm69hw_regs.h"
#include "rfm69hw_hal.h"

void ManualModuleReset();

void SetModuleAddress(uint8_t addr);

void SetModuleNetwork(uint8_t networkID);

void SetPacketLength(PacketLength thePacketLength);

void SetModulePowerLevel(uint8_t level);

void EditModuleRegister(uint8_t regAddress, uint8_t  );

void SendData(uint8_t* dataBuffer, uint8_t howMany);

uint8_t GetModuleTemperature();

uint8_t WriteModuleRegister(uint8_t regAddress, uint8_t regData);

uint8_t WriteModuleRegisterN(uint8_t regAddress, uint8_t *regData, uint8_t dataSize);

uint8_t ReadModuleRegister(uint8_t regAddress);

void SetModulePreAmbleLenLSB(uint8_t preAmbleLen);

void SetModulePreAmbleLenMSB(uint8_t preAmbleLen);

void SetModuleSyncWords(uint8_t *syncWordData, uint8_t howManySyncWords);

void SetModuleBitRate(BitRate theBitRate);



#endif
