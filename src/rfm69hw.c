#include "rfm69hw.h"

/**
 * Set the MSBit of register 0x37 to '0' for fixed length
 * or to '1' for variable length packet.
 * Refer to datasheet page 72.
 * @param packeLength
 */
void SetPacketLength(PacketLength thePacketLength){

}

/**
 * Performs module reset without removing VDD from the module.
 * See RFM69HW datasheet v1.3, paragraph: 7.2.2, page: 76.
 */
void ManualModuleReset(){

//    HAL_GPIO_WritePin(RFM_SPI_PORTx, _RFMHW_RESET_Pin, GPIO_PIN_SET);
//    osDelay(1);
//    HAL_GPIO_WritePin(RFM_SPI_PORTx, _RFMHW_RESET_Pin, GPIO_PIN_RESET);
//    osDelay(5);
}

/**
 * Reads the currently stored value on the specified register.
 * @param regAddress, the register to read data from.
 * @return, returns the register's value.
 */
uint8_t ReadModuleRegister(uint8_t regAddress){

    uint8_t regVal = 0;
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi3, &regAddress, 1, 1000);
    HAL_SPI_Receive(&hspi3, &regVal, 1, 1000);
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOC, _SPI3_RFMHW_MISO_Pin,  )

    return regVal;
}

/**
 * Writes the specified register with the specified data.
 * @param regAddress, the address to write to.
 * @param regData, the data for writing.
 * @return for now, always zero (0).
 */
uint8_t WriteModuleRegister(uint8_t regAddress, uint8_t regData){

    uint8_t regRead = 0;
    regRead = (regAddress | 0b10000000);//to change the first bit to '1' that means 'write access to the register'
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi3, &regRead, 1, 1000);
    HAL_SPI_Transmit(&hspi3, &regData, 1, 1000);
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);

    return 0;

}

/**
 * Write N size data to
 * @param regAddress
 * @param regData
 * @param dataSize
 * @return
 */
uint8_t WriteModuleRegisterN(uint8_t regAddress, uint8_t *regData, uint8_t dataSize){

    uint8_t regRead = 0;
    regRead = (regAddress | 0b10000000);//to change the first bit to '1' that means 'write access to the register'
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi3, &regRead, 1, 1000);

//    HAL_SPI_Transmit(&hspi3, regData, 1, 1000);

    for(uint8_t i=0; i<dataSize; i++){
        HAL_SPI_Transmit(&hspi3, regData+i, 1, 1000);
    }
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);

    return 0;
}

void SetModulePreAmbleLenLSB(uint8_t preAmbleLen){

    WriteModuleRegister(REGPREAMBLELSB_REG, preAmbleLen);

}

void SetModulePreAmbleLenMSB(uint8_t preAmbleLen){

    WriteModuleRegister(REGPREAMBLEMSB_REG, preAmbleLen);

}

void SetModuleSyncWords(uint8_t *syncWordData, uint8_t howManySyncWords){

    uint8_t syncRegs[8] = { REGSYNCVALUE1_REG, REGSYNCVALUE2_REG, REGSYNCVALUE3_REG, REGSYNCVALUE4_REG,
                            REGSYNCVALUE5_REG, REGSYNCVALUE6_REG, REGSYNCVALUE7_REG, REGSYNCVALUE8_REG
                          };

    for(uint8_t i=0; i<howManySyncWords; i++){
        WriteModuleRegister(syncRegs[i], syncWordData[i]);
    }

}

void SetModuleBitRate(BitRate theBitRate){

    switch(theBitRate){
    case kbps1_2:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x68);
        WriteModuleRegister(REGBITRATELSB_REG, 0x2B);
        break;
    case kbps2_4:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x34);
        WriteModuleRegister(REGBITRATELSB_REG, 0x15);
        break;
    case kbps4_8:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x1A);
        WriteModuleRegister(REGBITRATELSB_REG, 0x0B);
        break;
    case kbps9_6:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x0D);
        WriteModuleRegister(REGBITRATELSB_REG, 0x05);
        break;
    case kbps19_2:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x06);
        WriteModuleRegister(REGBITRATELSB_REG, 0x83);
        break;
    case kbps38_4:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x03);
        WriteModuleRegister(REGBITRATELSB_REG, 0x41);
        break;
    case kbps76_8:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x01);
        WriteModuleRegister(REGBITRATELSB_REG, 0x1A);
        break;
    case kbps153_6:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x00);
        WriteModuleRegister(REGBITRATELSB_REG, 0xDC);
        break;
    default:

        break;
    }

}
